﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRPO_Lab3.lib;

namespace Program
{

    class Program
    {

        static void Main(string[] args)
        {
            while (true)
            {

                Console.Write("Введите длину стороны a: ");
                double _a = Convert.ToDouble(Console.ReadLine());
                if (_a < 0)
                {
                    Console.WriteLine("Введите корректное значение.");
                    return;
                }
                Console.Write("Введите количество сторон n: ");
                double _n = Convert.ToDouble(Console.ReadLine());
                if (_n < 0)
                {
                    Console.WriteLine("Введите корректное значение.");
                    return;
                }

                RightPolygon RightPolygon = new RightPolygon();
                RightPolygon.Square(_a, _n);
            }
        }
    }
}
